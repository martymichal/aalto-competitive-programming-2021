use std::collections::HashMap;

fn main() {
    let reader = std::io::stdin();
    let mut buf = String::new();
    reader.read_line(&mut buf).unwrap();
    let num_lines = buf.trim().parse::<usize>().unwrap();

    let mut horizontal: Vec<((i64, i64), (i64, i64))> = Vec::new();
    let mut vertical: Vec<((i64, i64), (i64, i64))> = Vec::new();
    for _i in 0..num_lines {
        let (start, end): ((i64, i64), (i64, i64));
        let mut buf = String::new();
        reader.read_line(&mut buf).unwrap();

        let tmp: Vec<i64> = buf
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<i64>().unwrap())
            .collect();
        start = (tmp[0], tmp[1]);
        end = (tmp[2], tmp[3]);

        if start.0 == end.0 {
            vertical.push((start, end));
        } else {
            horizontal.push((start, end));
        }
    }

    horizontal.sort();
    vertical.sort();

    let mut num_intersections = 0;
    let mut tracking: HashMap<i64, i64> = HashMap::new();
    let mut to_end: HashMap<i64, i64> = HashMap::new();
    for line in horizontal {
        tracking.insert(line.0 .0, line.0 .1);
        to_end.insert(line.1 .0, line.1 .1);
    }

    for line in vertical {
        let mut to_remove: Vec<i64> = Vec::new();
        for (x, _y) in to_end.iter() {
            if line.0 .0 > *x {
                to_remove.push(x.clone());
            }
        }

        for val in to_remove {
            tracking.remove(&val);
            to_end.remove(&val);
        }

        for (_x, y) in &tracking {
            if (&line.0 .1 >= y && &line.1 .1 <= y) || (&line.0 .1 <= y && &line.1 .1 >= y) {
                num_intersections += 1;
            }
        }
    }

    println!("{}", num_intersections);
}
