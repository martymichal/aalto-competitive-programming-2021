tmp = input().strip().split()
num_val, num_q = int(tmp[0]), int(tmp[1])

vals = [int(val) for val in input().strip().split()]

sum_curr = 0
sum_prefix = []

for i in range(num_val):
    sum_curr = sum_curr + vals[i]
    sum_prefix.append(sum_curr)

for _ in range(num_q):
    tmp = input().split()
    left, right = int(tmp[0]), int(tmp[1])

    if left == 1:
        print(sum_prefix[right - 1])
    else:
        print(sum_prefix[right - 1] - sum_prefix[left - 2])
