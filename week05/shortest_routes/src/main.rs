use std::collections::VecDeque

fn dijkstra(graph: &Vec<Vec<usize>>, end: usize) -> usize {
    let queue: VecDeque<(usize, usize)> = VecDeque::new();

    let mut curr = 1;
    let mut dist = 0;


}

fn main() {
    let cities: usize;
    let connections: usize;

    let mut buf = String::new();
    let stdin = std::io::stdin();

    stdin.read_line(&mut buf).expect("couldn't read from stdin");
    let tmp: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();
    cities = tmp[0];
    connections = tmp[1];

    let mut graph: Vec<Vec<usize>> = vec![vec![]; cities + 1];
    for _i in 0..connections {
        let mut buf = String::new();
        stdin.read_line(&mut buf).expect("");
        let tmp: Vec<usize> = buf
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<usize>().unwrap())
            .collect();
        let len = tmp.len();
        for k in 1..len {
            let prev_num = tmp[k - 1];
            let curr_num = tmp[k];

            let prev = graph.get_mut(prev_num).unwrap();

            if !prev.contains(&curr_num) {
                prev.push(curr_num);
            }

            let curr = graph.get_mut(curr_num).unwrap();

            if !curr.contains(&prev_num) {
                curr.push(prev_num);
            }
        }
    }

    print!("0");
    for i in 2..=cities {
        let dist = dijkstra(&graph, i);
        print!(" {}", dist);
    }
}
