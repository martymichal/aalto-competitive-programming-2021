fn main() {
    let reader = std::io::stdin();
    let mut buf = String::new();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    let line: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    let books_num = line[0];
    let max_price = line[1];

    buf.clear();
    reader.read_line(&mut buf).expect("");
    let book_prices: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    buf.clear();
    reader.read_line(&mut buf).expect("");
    let book_pages = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    
}
