inp = input().split()

n = int(inp[2])

fj_minus_2 = int(inp[0])
fj_minus_1 = int(inp[1])
fj = None

if n == 1:
    fj = fj_minus_1

if n == 2:
    fj = fj_minus_2

for j in range(3, n+1):
    fj = (fj_minus_1 + fj_minus_2) % (10**9 + 7)

    fj_minus_2 = fj_minus_1
    fj_minus_1 = fj

print(fj)
