use std::collections::HashMap;

fn main() {
    let h: usize;
    let w: usize;

    let mut buf = String::new();
    let stdin = std::io::stdin();

    stdin.read_line(&mut buf).expect("couldn't read from stdin");
    let tmp: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();
    h = tmp[0];
    w = tmp[1];

    let mut dists: HashMap<usize, Vec<usize>> = HashMap::new();
    for _i in 0..h {
        let mut line = String::new();
        stdin.read_line(&mut line).expect("");
        let res = line.find(|c: char| c.is_digit(10));
        if res == None {
            continue;
        }

        let num = res.unwrap();
        let kayak = line.chars().nth(num).unwrap().to_digit(10).unwrap() as usize;
        if dists.contains_key(&num) {
            let val = dists.get_mut(&num).unwrap();
            val.push(kayak);
        } else {
            dists.insert(num, vec![kayak]);
        }
    }

    let mut rankings: HashMap<usize, usize> = HashMap::new();
    let mut rank = 1;
    for i in (1..w).rev() {
        if !dists.contains_key(&i) {
            continue;
        }

        let vals = dists.get(&i).unwrap();
        for k in vals {
            rankings.insert(*k, rank);
        }
        rank += 1;
    }

    for i in 1..=9 {
        println!("{}", rankings.get(&i).unwrap());
    }
}
