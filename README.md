# Competitive Programming 2021

- 1st period; winter semester
- Passed with 1/5
- In essence fun but easy to become frustrating due to bad scores caused by lack of speed and knowledge

## Quick Summary

Every week on Monday and Wednesday a contest for 1 hour and 30 minutes was held. During each contest several tasks were to be solved (3 for solo contest, 5 for group contest). Usually a topic or programming language were given in advance for students to prepare and some homework was also proposed.

The course required a fair knowledge of algorithms and mainly ones ability to quickly whip up a minimal needed implementation for a given problem. Large number of different algorithms were tried out and several programming languages could be used thorughout the course.

## Future

Revisiting some tasks and trying to finish them. Maybe even within a time limit.
