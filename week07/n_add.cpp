#include <chrono>
#include <iostream>
#include <map>
#include <random>
#include <unordered_map>
#include <vector>

int main() {
  long int res_i = 0;
  long double res_d = 0;

  // Addition
  auto start = std::chrono::system_clock::now();
  for (long int i = 0; i < 1'020'307'010; i++) {
    res_i += 314126;
  }

  printf("[ ADD ] %ld\n", res_i);
  auto end = std::chrono::system_clock::now();
  auto milliseconds =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  printf("[ ADD ] Time is %ld\n", milliseconds.count());

  // Multiplication
  start = std::chrono::system_clock::now();

  res_i = 1;
  for (long int i = 0; i < 1'370'307'020; i++) {
    res_i *= 12;
  }

  printf("[ MUL ] %ld\n", res_i);

  end = std::chrono::system_clock::now();
  milliseconds =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  printf("[ MUL ] Time is %ld\n", milliseconds.count());

  // Division
  start = std::chrono::system_clock::now();

  res_d = 1'000'000'000'000'000'000;
  for (long int i = 0; i < 542'030'700; i++) {
    res_d /= 2;
  }

  printf("[ DIV ] %Lf\n", res_d);

  end = std::chrono::system_clock::now();
  milliseconds =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  printf("[ DIV ] Time is %ld\n", milliseconds.count());

  // Map access and fetching
  start = std::chrono::system_clock::now();

  std::map<long int, long int> map;
  for (long int i = 0; i < 1'450'307; i++) {
    map[i] = i;
    long int val = map[i];
  }

  printf("[ MAP ] Size of map is %ld\n", map.size());
  end = std::chrono::system_clock::now();
  milliseconds =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  printf("[ MAP ] Time is %ld\n", milliseconds.count());

  // Unordered map access and fetching
  start = std::chrono::system_clock::now();

  std::unordered_map<long int, long int> unmap;
  for (long int i = 0; i < 9'150'307; i++) {
    unmap[i] = i;
    long int val = unmap[i];
  }

  printf("[UNMAP] Size of map is %ld\n", unmap.size());
  end = std::chrono::system_clock::now();
  milliseconds =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  printf("[UNMAP] Time is %ld\n", milliseconds.count());

  // Vector access and fetching
  start = std::chrono::system_clock::now();

  std::vector<long int> vec;
  for (long int i = 0; i < 70'450'307; i++) {
    vec.push_back(i);
    long int val = vec[i];
  }

  printf("[ VEC ] Size of vector is %ld\n", vec.size());
  end = std::chrono::system_clock::now();
  milliseconds =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  printf("[ VEC ] Time is %ld\n", milliseconds.count());

  // Random number generation
  std::mt19937 gen(start.time_since_epoch().count());

  start = std::chrono::system_clock::now();

  for (long int i = 0; i < 130'450'307; i++) {
    int val = gen();
  }

  printf("[ RAN ] Size of vector is %ld\n", vec.size());
  end = std::chrono::system_clock::now();
  milliseconds =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  printf("[ RAN ] Time is %ld\n", milliseconds.count());

  return 0;
}
