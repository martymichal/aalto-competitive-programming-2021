#include <iostream>
#include <vector>

class BigInt {
  std::vector<short> nums;
  bool negative = false;

public:
  BigInt(std::string text) {
    if (text[0] == '-') {
      negative = true;
      text = text.substr(1);
    }

    if (text[0] != '0') {
      for (auto i = text.begin(); i != text.end(); i++) {
        nums.push_back(*i - '0');
      }
    }
  }

  BigInt(size_t size) {
    negative = false;
    nums = std::vector<short>(size, 0);
  }

  BigInt operator*(const BigInt &other) {
    // Guard self assignment
    if (this == &other)
      return *this;

    if (this->nums.size() == 0 || other.nums.size() == 0) {
      return BigInt(0);
    }

    BigInt base = BigInt(this->nums.size() + other.nums.size());
    if ((this->negative && other.negative) ||
        (!this->negative && !other.negative)) {
      base.negative = false;
    } else {
      base.negative = true;
    }
    for (size_t i = 1; i <= other.nums.size(); i++) {
      int other_i = other.nums.size() - i;
      int carry = 0;

      // Multiplication as in elementary school
      for (size_t j = 1; j <= this->nums.size(); j++) {
        int this_i = this->nums.size() - j;

        printf("N: %d * %d\n", other.nums[other_i], this->nums[this_i]);

        int res = other.nums[other_i] * this->nums[this_i] + carry;
        int fin = res % 10;
        carry = (res - fin) % 10;

        base.nums[base.nums.size() - i - j + 1] += fin;
        printf("I: %ld\n", base.nums.size() - i - j + 1);
        printf("%d %d %d\n", res, fin, carry);
        base.print();
      }

      // Distribute the rest of the carry
      if (carry != 0) {
      }
    }

    return base;
  }

  void print() {
    if (this->negative && this->nums.size() != 0) {
      std::cout << "-";
    }

    if (this->nums.size() == 0) {
      std::cout << "0" << std::endl;
      return;
    }

    bool skip = true;
    for (size_t i = 0; i < this->nums.size(); i++) {
      if (this->nums.at(i) == 0 && skip) {
        continue;
      } else {
        skip = false;
      }
      std::cout << this->nums.at(i);
    }
    std::cout << std::endl;
  }
};

int main() {
  std::string a_s, b_s;

  std::cin >> a_s >> b_s;

  BigInt a = BigInt(a_s);
  BigInt b = BigInt(b_s);

  BigInt res = a * b;
  res.print();
}
