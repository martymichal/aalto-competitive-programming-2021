Hi all, our topic for the next week is C++. We will have C++-only practice contests, forcing you to learn at least the basics of the C++ programming language and its standard library.

If you don't know any C++ yet, please try to familiarize yourself enough with it so that you can solve tasks similar to those we have seen in the previous weeks.

The real homework, however, is to get some first-hand understanding of what you can do with C++ in 1 second. Try to answer these questions (using your own personal computer):

What is the largest n so that in 1 second you can…

    … do n additions?
    … do n multiplications?
    … do n divisions?
    … add & fetch n different elements in std::map<int,int>?
    … add & fetch n different elements in std::unordered_map<int,int>?
    … add & fetch n different elements in std::vector<int>?
    … sort an array of n integers with std::sort?
    … follow a linked list with n elements from start to end?
    … generate n random numbers with std::mt19937?

Please make sure you use meaningful optimization flags when compiling code for these experiments, like g++ -O2. Also, try to make sure your test data is meaningful (not e.g. sorting an array that is already sorted), and on the other hand also make sure your test data generation won't dominate your benchmarks. And make sure you are using the results of the computation (e.g. print out something), so that the compiler can't optimize everything away…
