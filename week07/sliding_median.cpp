#include <algorithm>
#include <deque>
#include <iostream>
#include <iterator>
#include <set>

size_t num_of, win_size, index;
std::multiset<int> window;
std::deque<int> win_nums;

void report() {
  auto print = [](const int &n) { std::cerr << n << " "; };
  std::cerr << "W: ";
  std::for_each(window.begin(), window.end(), print);
  std::cerr << "- N: ";
  std::for_each(win_nums.begin(), win_nums.end(), print);
  std::cerr << '\n';
  std::cerr.flush();
}

void find_median() {
  // report();

  auto it = window.begin();
  std::advance(it, index);

  std::cout << *it << " ";
  // std::cout << std::endl;
}

int main() {
  std::cin >> num_of >> win_size;

  // Prep index number
  index = win_size / 2;

  if (win_size % 2 == 0) {
    index -= 1;
  }

  for (size_t i = 0; i < win_size; i++) {
    int num;
    std::cin >> num;
    win_nums.push_back(num);
    window.insert(num);
    // report();
  }

  find_median();

  for (size_t i = 0; i < num_of - win_size; i++) {
    // Update the window
    int new_num;
    std::cin >> new_num;

    // report();
    int old_num = win_nums.front();
    window.extract(old_num);

    win_nums.push_back(new_num);
    window.insert(new_num);
    // The eldest number is no longer needed
    win_nums.pop_front();

    // report();
    find_median();
  }

  return 0;
}
