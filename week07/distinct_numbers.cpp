#include <iostream>
#include <set>
#include <string>

int main() {
    std::set<int> set;

    size_t num_of;
    std::cin >> num_of;

    for (size_t i = 0; i < num_of; i++) {
        int num;
        std::cin >> num;
        set.insert(num);
    }

    std::cout << set.size() << std::endl;
    return 0;
}
