#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll, ll> ii;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;

#define x first
#define y second
#define pb push_back
#define eb emplace_back
#define all(v) (v).begin(), (v).end()

void run() {
	ll n, k;
	cin >> n >> k;
	vi A(n, 0), B(n, 0), S(n, 0);
	unordered_map<string, ll> M;
	for (ll i = 0; i < n; i++) {
		string s;
		cin >> s >> S[i];
		M[s] = i;
	}
	ll l;
	cin >> l;
	ll res = 0;
	for (ll i = 0; i < l; i++) {
		string s; ll t;
		cin >> s >> t;
		ll j = M[s];
		if (t > S[j]) {
			k -= t-S[j];
			res += B[j] + (t-S[j])*A[j];
			B[j] = 0;
			S[j] = t;
		}
		A[j]++;
		if (t == S[j]) {
			B[j] += S[j];
		} else {
			res += S[j];
		}
	}
	if (k < 0) {
		cout << 0 << endl;
		return;
	}
	priority_queue<ii> Q;
	for (ll i = 0; i < n; i++) {
		Q.emplace(B[i]+A[i], i);
	}
	while (k > 0) {
		ii p = Q.top();
		Q.pop();
		if (B[p.y] > 0) {
			res += p.x; k--;
			B[p.y] = 0;
			Q.emplace(A[p.y], p.y);
		} else {
			res += p.x*k;
			k = 0;
		}
	}
	cout << res << endl;
}

signed main() {
    ios_base::sync_with_stdio(false); cin.tie(0);
    run();
    return 0;
}
