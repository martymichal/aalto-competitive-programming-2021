n, k = [int(i) for i in input().split()]
stats = {}
score = 0
for i in range(n):
    stat, lvl = input().split()
    stats[stat] = [int(lvl), 0, 0] # (current lvl, events at current lvl, total events)

for i in range(int(input())):
    stat, lvl = input().split()
    s = stats[stat]
    lvl = int(lvl)
    if lvl < s[0]:
        score += s[0]
    elif lvl == s[0]:
        s[1] += 1
    else:
        diff = lvl-s[0]
        score += s[1]*s[0] + s[2]*diff
        k -= diff
        s[0] = lvl
        s[1] = 1
    s[2] += 1

bonus = []
best_total = 0
for s in stats.values():
    bonus.append(s[1]*s[0] + s[2])
    best_total = max(best_total, s[2])

bonus.sort(reverse = True)
for i in bonus:
    if i<best_total or k <= 0:
        break
    score += i
    k -= 1
if k > 0:
    score += best_total * k
if k < 0:
    print(0)
else:
    print(score)
