import java.io.*;
import java.util.*;

public class ludo {
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String[] parts = in.readLine().split(" ");
		int n = Integer.valueOf(parts[0]);
		long k = Long.valueOf(parts[1]);

		String[] names = new String[n];
		long[] scores = new long[n];
		ArrayList[] thresholds = new ArrayList[n];
		HashMap<String, Integer> lookup = new HashMap<String, Integer>();

		for (int i=0; i<n; i++) {
			parts = in.readLine().split(" ");
			names[i] = parts[0];
			scores[i] = Long.valueOf(parts[1]);
			thresholds[i] = new ArrayList<Long>();
			lookup.put(names[i], i);
		}

		int l = Integer.valueOf(in.readLine());
		for (int i=0; i<l; i++) {
			parts = in.readLine().split(" ");
			int idx = lookup.get(parts[0]);
			thresholds[idx].add(Long.valueOf(parts[1]));
		}

		long answer = 0;
		long largest = 0;
		long[] activation = new long[n];
		for (int i=0; i<n; i++) {
			// if (thresholds[i].empty()) continue;
			largest = Math.max(largest, thresholds[i].size());
			long needed = 0;
			for (Long t : (ArrayList<Long>) thresholds[i])
				needed = Math.max(needed, t);
			if (needed > scores[i]) {
				k -= (needed - scores[i]);
				scores[i] = needed;
			}
			long cur_size = 0;
			activation[i] = thresholds[i].size();
			for (long t : (ArrayList<Long>) thresholds[i]) {
				if (t == scores[i])
					activation[i] += scores[i];
				else
					answer += scores[i];
			}
		}
		Arrays.sort(activation);
		for (int i = n-1; i >= 0 && activation[i] > largest && k > 0; i--) {
			answer += activation[i];
			k--;
		}
		answer += k * largest;

		if (k < 0) {
			System.out.println(0);
			return;
		}
		System.out.println(answer);
	}
}
