import java.io.*;
import java.util.*;

public class LudoUnderflow {
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String[] parts = in.readLine().split(" ");
		int n = Integer.valueOf(parts[0]), k = Integer.valueOf(parts[1]);

		String[] names = new String[n];
		int[] scores = new int[n];
		ArrayList[] thresholds = new ArrayList[n];
		HashMap<String, Integer> lookup = new HashMap<String, Integer>();

		for (int i=0; i<n; i++) {
			parts = in.readLine().split(" ");
			names[i] = parts[0];
			scores[i] = Integer.valueOf(parts[1]);
			thresholds[i] = new ArrayList<Integer>();
			lookup.put(names[i], i);
		}

		int l = Integer.valueOf(in.readLine());
		for (int i=0; i<l; i++) {
			parts = in.readLine().split(" ");
			int idx = lookup.get(parts[0]);
			thresholds[idx].add(Integer.valueOf(parts[1]));
		}

		long answer = 0;
		int largest = 0;
		int[] activation = new int[n];
		for (int i=0; i<n; i++) {
			// if (thresholds[i].empty()) continue;
			largest = Math.max(largest, thresholds[i].size());
			int needed = 0;
			for (Integer t : (ArrayList<Integer>) thresholds[i])
				needed = Math.max(needed, t);
			if (needed > scores[i]) {
				// WA: k can underflow
				k -= (needed - scores[i]);
				scores[i] = needed;
			}
			int cur_size = 0;
			activation[i] = thresholds[i].size();
			for (Integer t : (ArrayList<Integer>) thresholds[i]) {
				if (t == scores[i])
					activation[i] += scores[i];
				else
					answer += scores[i];
			}
		}
		Arrays.sort(activation);
		for (int i = n-1; i >= 0 && activation[i] > largest && k > 0; i--) {
			answer += activation[i];
			k--;
		}
		answer += ((long) k) * largest;
		if (k < 0) {
			System.out.println(0);
			return;
		}
		System.out.println(answer);
	}
}
