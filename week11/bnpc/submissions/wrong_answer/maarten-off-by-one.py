from collections import defaultdict

(n, k), (max_events, num_events, num_max) = [int(x) for x in input().split()], [defaultdict(int) for _ in range(3)]
attrs, l = {a: int(v) for a, v in (input().split() for _ in range(n))}, int(input())
events = [(a, int(v)) for a, v in (input().split() for _ in range(l))]

for a, v in events:
    num_events[a] += 1
    if v == max_events[a]: num_max[a] += 1
    if v > max_events[a]: max_events[a], num_max[a] = v, 1

# First, try to use attribute points so that all events are passed
k -= sum(v - attrs[a] for a, v in max_events.items() if attrs[a] < v)
attrs = {a: max(max_events[a], v) for a, v in attrs.items()}

# If we don't have enough attribute points, bail out
if k < 0: print(0), exit()

# For all events for which the skill level (non-zero) is currently exactly reached, add one attribute point.
# Start with the events that are the most lucrative, in case we don't have enough attribute points to increase them all.
queue = sorted((-v * num_max[a], a) for a, v in max_events.items() if v != 0 and attrs[a] == v)
for _, a in queue[:k]: attrs[a] += 1

# Add all remaining skill points to the event that happens the most often.
attrs[max(num_events.items(), key=lambda t: t[1])[0]] += max(0, k - len(queue))

print(sum(attrs[a] for a, v in events if attrs[a] > v))
