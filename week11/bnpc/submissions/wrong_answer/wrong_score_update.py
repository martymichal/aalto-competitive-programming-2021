scores = {}
occurences = {}

(n,k) = map(int, input().split())

for _ in range(n):
	attribute, score = input().split()
	score = int(score)
	scores[attribute] = score
	occurences[attribute] = 0

l = int(input())

for _ in range(l):
	attribute, threshold = input().split()
	threshold = int(threshold)
	occurences[attribute] += 1
	points_to_spend = max(0, threshold - scores[attribute])
	k -= points_to_spend
	scores[attribute] += points_to_spend

max_score = 0
priority_list = []

for attribute, occurence in occurences.items():
	priority_list.append(((scores[attribute] + 1) * occurences[attribute], 1))
	priority_list.append((occurences[attribute], k))

priority_list.sort(reverse = True)

index = 0
while k > 0:
	score_increase, potential_points_to_spend = priority_list[index]
	points_to_spend = min(k, potential_points_to_spend)
	k -= points_to_spend
	max_score += score_increase * points_to_spend
	index += 1

print(max_score)
