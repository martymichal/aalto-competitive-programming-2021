#!/usr/bin/env python3
from collections import defaultdict

n, k = map(int, input().split())
scores = dict()
for _ in range(n):
    e, s = input().split()
    scores[e] = int(s)

e = int(input())
events = []
first_point = defaultdict(int)
num_events = defaultdict(int)
for _ in range(e):
    e, s = input().split()
    s = int(s)
    events.append((e, s))

    num_events[e] += 1

    # Handle required points to stay alive
    if scores[e] < s:
        k -= s - scores[e]
        scores[e] = s
        first_point[e] = s
    # WRONG_ANSWER: Should increment first point when matching events are found.
    # elif scores[e] == s:
    # first_point[e] += s

for e in first_point:
    first_point[e] += num_events[e]

if k < 0:
    print(0)
    exit(0)

score = 0
for e, s in events:
    if scores[e] > s:
        score += scores[e]
    elif scores[e] == s:
        pass
    else:
        assert False

max_per_point = max(num_events.values())

first_points = sorted(first_point.items(), reverse=True, key=lambda kv: kv[1])

for e, s in first_points:
    if k == 0:
        break
    if s < max_per_point:
        break
    k -= 1
    score += s

score += k * max_per_point
print(score)
