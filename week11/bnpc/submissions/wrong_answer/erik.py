n, k = map(int, input().split(" "))

score = {}
count = {}
req = {}
for _ in range(n):
    line = input().split(" ")
    attr = line[0]
    score[attr] = int(line[1])
    count[attr] = 0
    req[attr] = 0

l = int(input())
for _ in range(l):
    line = input().split(" ")
    attr = line[0]
    thr = int(line[1])
    count[attr] += 1
    req[attr] = max(req[attr], thr)

options = []
ans = 0

for attr in score:
    if score[attr] <= req[attr]:
        k -= (req[attr] - score[attr])
        score[attr] = req[attr]
        options.append((req[attr] + 1) * count[attr])
    else:  # already getting a bunch of points from this attribute
        ans += score[attr] * count[attr]

if k < 0:  # didnt have enough points to spend to reach the minimum for all events
    print(0)
else:
    max_count = max(count.values())
    options = reversed(sorted(options))
    for opt in options:
        if opt <= max_count:
            break
        if k == 0:
            break
        ans += opt
        k -= 1
    ans += k * max_count

    print(ans)


