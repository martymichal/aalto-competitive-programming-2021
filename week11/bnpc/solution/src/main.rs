use std::collections::{BTreeMap, HashMap};

fn main() {
    let reader = std::io::stdin();

    let mut buf = String::new();
    reader.read_line(&mut buf).unwrap();
    let nums: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    let num_attributes = nums[0];
    let mut points_left = nums[1];

    let mut attributes: HashMap<String, usize> = HashMap::new();
    for _i in 0..num_attributes {
        let mut buf = String::new();
        reader.read_line(&mut buf).unwrap();
        let tmp: Vec<&str> = buf.trim().split_whitespace().collect();

        attributes.insert(tmp[0].to_string(), tmp[1].parse::<usize>().unwrap());
    }

    let mut buf = String::new();
    reader.read_line(&mut buf).unwrap();

    let mut score = 0;

    let num_events = buf.trim().parse::<usize>().unwrap();
    let mut events: BTreeMap<String, usize> = BTreeMap::new();
    for _i in 0..num_events {
        let mut buf = String::new();
        reader.read_line(&mut buf).unwrap();
        let tmp: Vec<&str> = buf.trim().split_whitespace().collect();

        let val = tmp[1].parse::<usize>().unwrap();
        events.insert(tmp[0].to_string(), val);
    }

    println!("{}", score);
}
