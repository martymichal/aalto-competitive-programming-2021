#include <bits/stdc++.h>
using namespace std;

constexpr long long MINUS_INF = -1000LL * 1000LL * 1000LL * 1000LL * 1000LL * 1000LL;

int main() {
	int n;
	cin >> n;
	vector<int> a(n);
	for(int& v : a) cin >> v;
	reverse(begin(a), end(a));

	vector<long long> dp(n, MINUS_INF);
	dp[0] = a[0];
	for(int jump_size = 1; jump_size < n; ++jump_size)
		for(int s = 0; s + jump_size < n; ++s)
			if(dp[s] > MINUS_INF)
				dp[s + jump_size] = max(dp[s + jump_size], dp[s] + a[s + jump_size]);

	cout << dp.back() << endl;
}
