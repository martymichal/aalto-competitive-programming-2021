n, a = int(input()), list(reversed(list(map(int, input().split()))))
memo = [[a[0]] * n, *([-(2 ** 42)] * n for pos in range(1, n))]

for pos in range(1, n):
    for speed in range(1, n):
        memo[pos][speed] = max(memo[pos][speed - 1], -(2 ** 42) if speed > pos else memo[pos - speed][speed] + a[pos])

print(memo[-1][-1])
