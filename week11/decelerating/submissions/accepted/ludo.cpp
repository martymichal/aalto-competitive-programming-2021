#include <iostream>
using namespace std;
typedef long long ll;

ll N, A[3000], DP[3000][3000];

int main() {
	cin >> N;
	for(int i = 0; i < N; i++) cin >> A[N - 1 - i];
	for(int gap = 1; gap < N; gap++) DP[0][gap] = A[0];
	for(int i = 1; i < N; i++) {
		DP[i][0] = -1e15;
		for(int gap = 1; gap < N; gap++)
			DP[i][gap] = gap > i ? DP[i][i] : max(DP[i][gap - 1], DP[i - gap][gap] + A[i]);
	}
	cout << DP[N - 1][N - 1] << endl;
	return 0;
}
