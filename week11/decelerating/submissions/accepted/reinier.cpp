#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll, ll> ii;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;

#define x first
#define y second
#define pb push_back
#define eb emplace_back
#define all(v) (v).begin(), (v).end()

void run() {
	ll n;
	cin >> n;
	vi A(n);
	for(ll i = 0; i < n; i++) cin >> A[n - 1 - i];
	vvi T(n, vi(n + 1, 0));
	for(ll i = n - 1; i >= 0; i--) {
		for(ll j = n; j >= 0; j--) {
			if(i == n - 1) {
				T[i][j] = A[i];
			} else if(i + j >= n) {
				T[i][j] = -1'000'000'000'000'000'000;
			} else {
				T[i][j] = A[i] + T[i + j][j];
				T[i][j] = max(T[i][j], T[i][j + 1]);
			}
		}
	}
	cout << T[0][1] << endl;
}

signed main() {
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	run();
	return 0;
}
