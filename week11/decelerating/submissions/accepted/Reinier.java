import java.util.*;

public class Reinier {
    public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		long[] A = new long[n];
		for (int i = 0; i < n; i++) {
			A[n-1-i] = sc.nextLong();
		}
		long[][] T = new long[n][n+1];
		for (int i = n-1; i >= 0; i--) {
			for (int j = n; j >= 0; j--) {
				if (i == n-1) {
					T[i][j] = A[i];
				} else if (i+j >= n) {
					T[i][j] = -1000000000000000000L;
				} else {
					T[i][j] = A[i] + T[i+j][j];
					T[i][j] = Math.max(T[i][j], T[i][j+1]);
				}
			}
		}
		System.out.println(T[0][1]);
    }
}
