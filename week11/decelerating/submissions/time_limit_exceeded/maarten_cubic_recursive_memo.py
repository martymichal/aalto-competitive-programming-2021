import sys

sys.setrecursionlimit(100000)

n, a = int(input()), list(reversed(list(map(int, input().split()))))
memo = dict()


def jump(pos, speed):
    if (pos, speed) in memo:
        return memo[(pos, speed)]
    ans = a[pos] + (
        0
        if pos == n - 1
        else max((jump(i, i - pos) for i in range(pos + speed, n)), default=-(2 ** 42))
    )
    memo[(pos, speed)] = ans
    return ans


print(jump(0, 1))
