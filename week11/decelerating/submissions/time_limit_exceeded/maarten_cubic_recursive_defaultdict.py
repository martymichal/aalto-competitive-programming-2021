from collections import defaultdict
import sys

sys.setrecursionlimit(10000)


# Inspired by https://stackoverflow.com/a/2912455
class keydefaultdict(defaultdict):
    def __missing__(self, key):
        ret = self[key] = self.default_factory(key)
        return ret


n, a = int(input()), list(reversed(list(map(int, input().split()))))
memo = keydefaultdict(
    lambda key: a[key[0]]
    + (
        0
        if key[0] == n - 1
        else max(
            (memo[(i, i - key[0])] for i in range(key[0] + key[1], n)),
            default=-(2 ** 42),
        )
    )
)
print(memo[(0, 1)])
