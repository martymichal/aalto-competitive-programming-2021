n, a = int(input()), list(reversed(list(map(int, input().split()))))
memo = [*[0] * (n - 1), [a[-1]] * n]

# for pos in reversed(range(n - 1)):
#     for speed in range(1, n - pos):
#         memo[pos][speed] = a[pos] + max((memo[pos + i][i] for i in range(speed, n - pos)), default=-2 ** 42)
for x in reversed(range(n - 1)):
    memo[x] = [
        None,
        *(a[x] + max(memo[x + i][i] for i in range(v, n - x)) for v in range(1, n - x)),
        *[-(2 ** 42)] * x,
    ]

print(memo[0][1])
