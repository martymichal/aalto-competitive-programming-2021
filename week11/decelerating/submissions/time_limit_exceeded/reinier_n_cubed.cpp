#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef pair<ll, ll> ii;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef vector<ii> vii;

#define x first
#define y second
#define pb push_back
#define eb emplace_back
#define all(v) (v).begin(), (v).end()

const ll MINUS_INFTY = -1'000'000'000'000'000'000;

void run() {
	ll n;
	cin >> n;
	vi A(n);
	for (ll i = 0; i < n; i++) cin >> A[i];
	vvi T(n, vi(n + 1, 0));
	for (ll i = 0; i < n; i++) {
		for(ll j = 1; j <= n; j++) {
			if (i == 0) {
				T[i][j] = A[0];
			} else {
				T[i][j] = MINUS_INFTY;
				for (ll k = 0; k <= i-j; k++) {
					T[i][j] = max(T[i][j], T[k][i-k]);
				}
				T[i][j] += A[i];
			}
		}
	}
	cout << T[n-1][1] << endl;
}

signed main() {
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	run();
	return 0;
}
