import java.io.*;
import java.util.*;

public class MaartenCubicIterative {
    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            new MaartenCubicIterative().run(reader);
        }
    }

    public void run(BufferedReader reader) throws IOException {
        int n = Integer.parseInt(reader.readLine());

        long[] a = new long[n];
        String[] xs = reader.readLine().split(" ");
        for (int i = 0; i < n; i++) {
            // Reverse the input
            a[n-1-i] = Long.parseLong(xs[i]);
        }

        long[][] memo = new long[n][n];
        for (int i = 1; i < n; i++) {
            memo[n - 1][i] = a[n - 1];
        }

        for (int pos = n - 2; pos >= 0; pos--) {
            for (int speed = 1; speed < n - pos; speed++) {
                long ans = -1L << 42;
                // ans = max((memo[pos + i][i] for i in range(speed, n - pos)), default=-2 ** 42)
                for (int i = speed; i < n - pos; i++) {
                    ans = Long.max(ans, memo[pos + i][i]);
                }
                memo[pos][speed] = a[pos] + ans;
            }
            for (int speed = n - pos; speed < n; speed++) {
                memo[pos][speed] = -1L << 42;
            }
        }

        System.out.println(memo[0][1]);
    }
}
