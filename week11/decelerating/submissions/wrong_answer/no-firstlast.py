#!/usr/bin/env python3

n = int(input())

l = list(reversed(list(map(int, input().split()))))
v = [-(10 ** 18)] * n
v[0] = l[0]
for s in range(1, n):
    for i in range(s, n):
        v[i] = max(v[i], v[i - s] + l[i])
print(v[-1] - l[0] - l[-1])
