const A: i128 = 9113482;
const B: i128 = 9726653;

fn process(text: &str) -> (Vec<i128>, Vec<i128>) {
    let iter = text.chars().enumerate();
    let prefixes: Vec<i128> = iter
        .scan(0, |acc, (i, c)| {
            if i == 0 {
                *acc = c as i128;
            } else {
                *acc = (*acc * A + c as i128).rem_euclid(B);
            }
            Some(*acc)
        })
        .collect();

    let iter = text.chars().enumerate();
    let base: Vec<i128> = iter
        .scan(1, |acc, (i, _c)| {
            if i == 0 {
                *acc = 1;
            } else {
                *acc = (*acc * A).rem_euclid(B);
            }
            Some(*acc)
        })
        .collect();

    (prefixes, base)
}

fn occurrences(base: (Vec<i128>, Vec<i128>), pat: (Vec<i128>, Vec<i128>), pat_len: usize) -> usize {
    let mut found = 0;
    let target = pat.0[pat_len - 1];

    let len = base.0.len();
    for i in 0..=len - pat_len {
        let hash;
        if i == 0 {
            hash = base.0[i + pat_len - 1];
        } else {
            let right = base.0[i + pat_len - 1];
            let left = base.0[i - 1];
            let base = base.1[i + pat_len - 1 - i + 1];
            hash = (right - left * base).rem_euclid(B);
        }

        if hash == target {
            found += 1;
        }
    }

    found
}

fn main() {
    let reader = std::io::stdin();
    let mut buf = String::new();

    reader.read_line(&mut buf).unwrap();
    let text = buf.trim().clone();

    let mut buf = String::new();
    reader.read_line(&mut buf).unwrap();
    let pat = buf.trim();

    let base = process(text);
    let find = process(pat);

    println!("{}", occurrences(base, find, pat.len()));
}
