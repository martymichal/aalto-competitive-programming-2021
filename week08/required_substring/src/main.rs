const NUM_OF_CHARS: i32 = 26; // Number of upper-case ASCII letters

fn main() {
    let reader = std::io::stdin();

    let mut buf = String::new();
    reader.read_line(&mut buf).unwrap();
    let target_len = buf.trim().parse::<usize>().unwrap();

    let mut buf = String::new();
    reader.read_line(&mut buf).unwrap();
    let input = buf.trim();
    let base_len = input.len();

    let len_diff: i32 = target_len as i32 - base_len as i32;
    if len_diff <= 0 {
        println!("0");
    } else {
        let res: i32 = (NUM_OF_CHARS - 1).pow(len_diff as u32) * (len_diff + 1);
        println!("{}", res.rem_euclid((10 as i32).pow(9 as u32) + 7));
    }
}
