use std::io::{self, Read};

fn main() {
    let mut buf = String::new();
    let mut stdin = io::stdin();

    stdin
        .read_to_string(&mut buf)
        .expect("could not read from stdin");
    let line: &str = buf.trim();
    let parts: Vec<&str> = line.split(' ').collect();
    assert_eq!(parts.len(), 2);

    let a: i32 = parts[0].parse::<i32>().unwrap();
    let b: i32 = parts[1].parse::<i32>().unwrap();

    let res = a + b;
    println!("{}", res);
}
