#!/usr/bin/env python3

import sys


def diff_chars(work, end):
    num = 0
    for i, _ in enumerate(work):
        if work[i] != end[i]:
            num += 1

    return num


def switcharoo(work, end, positions):
    res = ""
    for i, _ in enumerate(work):
        c = work[i]

        if len(positions) == 0 or i != positions[0]:
            res += c
            continue

        positions = positions[1:]
        c = end[i]

        if (i != 0 and work[i - 1] == c) or (i + 1 < len(work) and work[i + 1] == c):
            if c == "Z":
                c = chr(ord(c) - 1)
            else:
                c = chr(ord(c) + 1)

        res += c

    return res


def main():
    work = input()
    end = input()

    if len(work) != len(end):
        sys.exit(1)

    print(work, "\n")

    last_positions = []
    replay = False
    while work != end:
        positions = []
        skip_num = 0

        while True:
            new_positions = []
            skipped = 0
            found_diff = False

            for i, _ in enumerate(work):
                if work[i] != end[i] and (
                    len(new_positions) == 0
                    or (len(new_positions) != 0 and new_positions[-1] != i - 1)
                ):
                    print("REP ", replay)
                    if replay and not found_diff and i in last_positions:
                        found_diff = True
                        continue

                    if skipped < skip_num:
                        skipped += 1
                        continue

                    new_positions.append(i)
                    print(new_positions)

            if len(new_positions) > len(positions):
                positions = new_positions
                skip_num += 1
            else:
                break

        diff_pre = diff_chars(work, end)
        work = switcharoo(work, end, positions)
        diff_post = diff_chars(work, end)

        if not replay:
            last_positions = positions

        if diff_post >= diff_pre and not replay:
            replay = True
            continue
        elif diff_post >= diff_pre and replay:
            work = switcharoo(work, end, last_positions)
            last_positions = positions

        print(work, "\n")
        replay = False


if __name__ == "__main__":
    main()
