use std::io::Write;

fn main() {
    let (mut go_west, mut go_east): (bool, bool) = (false, false);
    let mut room: i32 = 0;
    let mut rooms: Vec<i32> = Vec::new();

    while !go_west || !go_east {
        let mut buf: String = String::new();
        let mut direction: char = ' ';

        std::io::stdin().read_line(&mut buf).expect("couldn't read from stdin");
        buf = buf.trim().to_string();

        rooms.push(room);

        if buf.eq("E"){
            go_east = true;
        }
        if buf.eq("W") {
            go_west = true;
        }

        if !go_west {
            room += 1;
            direction = 'E';
        } else if !go_east {
            room -= 1;
            direction = 'W';
        }

        if direction != ' ' {
            println!("{}", direction);
            std::io::stdout().flush().expect("couldn't flush stdout");
        }
    }

    rooms.sort();
    rooms.dedup();
    println!("{}", rooms.len());
}
