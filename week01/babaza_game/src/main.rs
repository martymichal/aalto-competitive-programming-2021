fn is_babaza(text: &str) -> bool {
    let chars: Vec<char> = text.chars().collect();
    for c in chars {
        if !c.is_ascii_alphabetic() {
            return false;
        };
        if !c.is_uppercase() {
            return false;
        };
    }
    true
}

fn get_indexes(work: &str, end: &str, variant: i32) -> &'static [i32] {
    let mut indexes: Vec<i32> = Vec::new();

    let work_chars: Vec<char> = work.chars().collect();
    let end_chars: Vec<char> = work.chars().collect();
    for i in 0..work_chars.len() {
        if work_chars[i] != end_chars[i] {
            if indexes.len() == 0 || indexes[indexes.len() - 1] != i - 1 {
                indexes.push(i as i32);
            }
        }
    }

    &indexes
}

fn switcharoo(work: &mut String, end: &str, indexes: &'static [i32]) {}

fn main() {
    let (mut work, mut end) = (String::new(), String::new());
    let stdin: std::io::Stdin = std::io::stdin();

    stdin.read_line(&mut work).expect("read from stdio failed");
    work = work.trim().to_string();
    stdin.read_line(&mut end).expect("read from stdio failed");
    end = end.trim().to_string();

    assert_eq!(work.len(), end.len());

    if !is_babaza(&work) || !is_babaza(&end) {
        std::process::exit(1);
    }

    while work.ne(&end) {
        let indexes: &'static [i32] = get_indexes(&work, &end, 0);
        switcharoo(&mut work, &end, &indexes);
    }
}
