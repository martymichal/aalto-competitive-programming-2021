fn main() {
    let reader = std::io::stdin();
    let mut buf = String::new();

    reader.read_line(&mut buf).unwrap();
    let num = buf.trim().parse::<i64>().unwrap();

    let mut cords: Vec<(i64, i64)> = Vec::new();
    for _i in 0..num {
        let mut buf = String::new();
        reader.read_line(&mut buf).unwrap();

        let p: Vec<i64> = buf
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<i64>().unwrap())
            .collect();

        let t: (i64, i64) = (p[0], p[1]);
        cords.push(t);
    }

    cords.sort();

    let mut touching: Vec<(i64, i64)> = Vec::new();
    let mut upper: Vec<(i64, i64)> = Vec::new();
    let mut lower: Vec<(i64, i64)> = Vec::new();
    for c in cords.iter() {
        println!("N: {:?}", c);
        upper.push(*c);
        lower.push(*c);

        if upper.len() < 3 || lower.len() < 3 {
            continue;
        }

        // Upper hull
        let ulen = upper.len();
        let udiff0 = upper[ulen - 1].0 - upper[ulen - 2].0;
        let udiff1 = upper[ulen - 1].1 - upper[ulen - 2].1;
        if udiff0 != udiff1
            && upper[ulen - 1].0 != upper[ulen - 2].0
            && upper[ulen - 1].1 >= upper[ulen - 2].1
        {
            println!("U: {:?}", c);
            upper.pop();
            upper.pop();
            upper.push(*c);
        }
        // Lower hull
        let llen = lower.len();
        let ldiff0 = lower[llen - 1].0 - lower[llen - 2].0;
        let ldiff1 = lower[llen - 1].1 - lower[llen - 2].1;
        if ldiff0 != ldiff1
            && lower[llen - 1].0 != lower[llen - 2].0
            && lower[llen - 1].1 <= lower[llen - 2].1
        {
            println!("L: {:?}", c);
            lower.pop();
            lower.pop();
            lower.push(*c);
        }
    }

    for c in upper.iter() {
        touching.push(*c);
    }

    for c in lower.iter() {
        touching.push(*c);
    }

    touching.sort();
    touching.dedup();

    println!("{}", touching.len());
    for c in touching.iter() {
        println!("{} {}", c.0, c.1);
    }
}
