fn main() {
    let reader = std::io::stdin();
    let mut buf = String::new();

    reader.read_line(&mut buf).unwrap();
    let num = buf.trim().parse::<usize>().unwrap();

    buf.clear();
    let mut largest = 0;
    for _i in 0..num {
        let mut buf = String::new();
        reader.read_line(&mut buf).unwrap();

        let p: Vec<usize> = buf
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<usize>().unwrap())
            .collect();

        let num = p[0] + p[1];
        if num > largest {
            largest = num;
        }
    }

    println!("{}", largest);
}
