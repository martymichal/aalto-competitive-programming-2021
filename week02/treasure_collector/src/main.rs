fn main() {
    let mut buf = String::new();

    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");

    let values: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    let n = values[0];
    let num_of_treasures = values[1];

    let mut treasures: Vec<Vec<usize>> = vec![vec![]; n + 1];

    for _i in 0..num_of_treasures {
        buf.clear();
        std::io::stdin()
            .read_line(&mut buf)
            .expect("couldn't read from stdin");

        let values: Vec<usize> = buf
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<usize>().unwrap())
            .collect();

        treasures[values[0]].push(values[1]);
    }

    let mut max_width = 0;
    for row in treasures {
        let width;
        let mut min: usize = usize::MAX;
        let mut max: usize = usize::MAX;

        for col in row {
            if min == usize::MAX && max == usize::MAX {
                min = col;
                max = col;
                continue;
            }

            if col < min {
                min = col;
            } else if col > max {
                max = col;
            }
        }

        width = max - min + 1;

        if width > max_width {
            max_width = width;
        }
    }

    println!("{}", max_width);
}
