use std::collections::HashMap;

fn search_routine(
    mut i: usize,
    target: &str,
    size: usize,
    cache: &HashMap<usize, String>,
) -> std::cmp::Ordering {
    let mut buf = String::new();
    let res: &str;

    if !cache.contains_key(&i) {
        let b = i / size.pow(4);
        i -= b * size.pow(4);

        let f = i / size.pow(3);
        i -= f * size.pow(3);

        let a = i / size.pow(2);
        i -= a * size.pow(2);

        let s = i / size;
        let p = i - s * size;

        println!("FETCH {}.{}.{}.{}.{}", b + 1, f + 1, a + 1, s + 1, p + 1);

        std::io::stdin()
            .read_line(&mut buf)
            .expect("couldn't read from stdin");
        res = buf.trim().trim_start_matches("FOUND").trim();
    } else {
        res = cache.get(&i).unwrap();
    }

    res.cmp(target)
}

fn main() {
    let mut buf = String::new();
    let size: usize;
    let book: &str;

    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    size = buf
        .trim()
        .trim_start_matches("SIZE")
        .trim()
        .parse::<usize>()
        .unwrap();

    buf.clear();
    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    book = buf.trim().trim_start_matches("COUNT").trim();

    let cache: HashMap<usize, String> = HashMap::new();
    let max_size = size.pow(5) - 1;
    let mut left: usize = 0;
    let mut step = max_size / 2;
    while step > 0 {
        while step > 0
            && (left + step) < max_size
            && search_routine(left + step, book, size, &cache) == std::cmp::Ordering::Less
        {
            left += step;
        }

        step /= 2;
    }

    let mut right: usize = max_size;
    step = (max_size + left) / 2;
    while step > 0 {
        while step > 0
            && (right as i32 - step as i32) > 0
            && search_routine(right - step, book, size, &cache) == std::cmp::Ordering::Greater
        {
            right -= step;
        }

        step /= 2;
    }

    let res: i32;
    if left == right {
        if search_routine(left, book, size, &cache) == std::cmp::Ordering::Equal {
            res = 1;
        } else {
            res = 0;
        }
    } else if left == 0 && right == max_size {
        res = max_size as i32 + 1;
    } else {
        res = right as i32 - left as i32 - 1;
    }
    println!("GOT {}", res);
}
