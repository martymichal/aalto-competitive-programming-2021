The topic of week 2 will be searching, and more precisely we will have at least some problems where the knowledge of these basic search techniques is going to be highly useful:

    binary search (for a sorted list)
    depth-first search (for a graph)
    breadth-first search (for a graph)

If you are unfamiliar with these, have a first quick refresher on the concepts. For example, you can look at the "Competitive Programmer's Handbook" at https://cses.fi/book/book.pdf and there Sections 3.3, 12.1, and 12.2.

But I think most of you are already somewhat familiar with these ideas. The real homework is to actually implement these correctly.

It is up to you to decide exactly what kind of an implementations to write and how to ensure they are working correctly, but I would suggest that you write the following computer programs (using your favorite programming language):

Program 1: First write a function that, given a sorted list of n elements and some value x, returns how many elements are equal to x. The function should work fast even if n is huge and even if there are very many elements equal to x (milliseconds for arrays with millions of elements). Then write a test program that calls the function with various test inputs (including inputs with zero, one, and many hits), and make sure it works both correctly and quickly.

Program 2: Write a complete program that reads in a (simple, undirected) graph G = (V,E) that is given as a list of edges. Then the program should do first a DFS traversal of the graph, starting with node 1, and print out the nodes in the DFS order. Then the program should do a BFS traversal of the graph, again starting with node 1, and print out the nodes in the BFS order. Prepare sufficiently interesting test inputs so that you can verify with pen and paper that the program is really doing what it is supposed to do, also in all kinds of corner cases. Also make sure the program works efficiently even if you have e.g. a graph with 1 million nodes and 10 million edges. Write another program that generates test inputs and see what happens. Note that you can't here use an adjacency matrix representation for such a large graph!

You do not need to send these programs to anyone. But in our contests in week 2 you will likely be very happy that you went through the trouble of writing this code. You will likely be able to use some code snippets directly in our tasks, or at least you have already the right mindset for solving the problems and remember what to be aware of when implementing something like binary search, DFS, and BFS. Already the basic thing of how to represent graphs in a computer program is a nontrivial thing that requires some practice.

And trust me: if you just read about binary search, DFS, and BFS, and think you understand the idea but don't actually try to implement them, you will be in deep trouble in our contests, most likely spending an hour of valuable contest time trying to get some ±1 indexing errors in binary search right, or something like that.

In the long run, you'll eventually also accumulate a useful library of commonly used code snippets that will help you get things done quickly. This can be a good start for that!
