use std::collections::VecDeque;

fn main() {
    let mut buf = String::new();
    let (node_num, edge_num, moped_width, car_width);

    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    let values: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    assert_eq!(values.len(), 4);
    node_num = values[0];
    edge_num = values[1];
    moped_width = values[2];
    car_width = values[3];

    let mut graph: Vec<(Vec<(usize, usize)>, i32)> = vec![(vec![], -1); node_num + 1];

    for _i in 1..=edge_num {
        buf.clear();
        std::io::stdin()
            .read_line(&mut buf)
            .expect("couldn't read from stdin");
        let vals: Vec<usize> = buf
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<usize>().unwrap())
            .collect();
        assert_eq!(vals.len(), 3);

        if moped_width > vals[2] {
            continue;
        }

        graph[vals[0]].0.push((vals[1], vals[2]));
        graph[vals[1]].0.push((vals[0], vals[2]));

        // If an excape route, make an edge from the default escape node
        if vals[2] >= moped_width && vals[2] < car_width {
            graph[0].0.push((vals[0], vals[2]));
            graph[0].0.push((vals[1], vals[2]));
        }
    }

    let mut to_visit: VecDeque<(usize, usize)> = VecDeque::new();
    let mut visited: Vec<usize> = Vec::new();
    let mut length = 0;

    to_visit.push_back((0, length));

    // println!();
    while to_visit.len() > 0 {
        let curr = to_visit.pop_front().unwrap();
        // println!("NOW: {} - {}", curr.0, curr.1);
        // println!("{:?}", visited);

        if visited.contains(&curr.0) {
            // println!("BEEN HERE");
            continue;
        }

        length = curr.1;
        graph[curr.0].1 = length as i32;
        visited.push(curr.0);

        for edge in &graph[curr.0].0 {
            if visited.contains(&edge.0) {
                continue;
            }

            if moped_width > edge.1 {
                continue;
            }
            // println!("{} - {}", edge.0, length);
            to_visit.push_back((edge.0, length + 1));
        }
        // println!("{:?}", to_visit);
    }

    for i in 1..=node_num {
        println!("{}", graph[i].1);
    }
}
