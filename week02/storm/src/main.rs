fn main() {
    let num_of_columns: usize;

    let mut buf: String = String::new();
    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    num_of_columns = buf.trim().parse::<usize>().unwrap();

    buf.clear();
    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    let columns: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse().unwrap())
        .collect();

    let mut res = 0;
    let mut max = 0;
    for i in 0..num_of_columns {
        max = std::cmp::max(max, columns[i]);
        res += max;
    }
    let mut max2 = 0;
    for i in (0..num_of_columns).rev() {
        max2 = std::cmp::max(max2, columns[i]);
        res -= max - max2;
    }
    println!("{}", res);
}
