#![feature(test)]

fn binary_search_range(list: &Vec<i32>, val: i32) -> i64 {
    if list.len() == 0 {
        return 0;
    }

    let (mut left, mut right) = (0, list.len() - 1);
    let (mut left_step, mut right_step) = (list.len() / 2, list.len() / 2);

    while left_step > 0 || right_step > 0 {
        while left + left_step < list.len() && list[left + left_step] < val {
            left += left_step;
        }

        while right as i32 - right_step as i32 > 0 && list[right - right_step] > val {
            right -= right_step;
        }

        left_step /= 2;
        right_step /= 2;
    }

    return right as i64 - left as i64 - 1;
}

fn main() {
    let mut input_list: Vec<i32> = Vec::new();
    let input_list_len: usize;
    let val: i32;

    let mut buf: String = String::new();
    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    input_list_len = buf.trim().parse::<usize>().unwrap();
    input_list.reserve(input_list_len);

    for _i in 0..input_list_len {
        buf.clear();
        std::io::stdin()
            .read_line(&mut buf)
            .expect("couldn't read from stdin");
        input_list.push(buf.trim().parse::<i32>().unwrap());
    }
    input_list.sort();
    println!("{:?}", input_list);

    buf.clear();
    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    val = buf.trim().parse::<i32>().unwrap();

    let found = binary_search_range(&input_list, val);

    println!("{}", found);
}

#[allow(soft_unstable)]
#[cfg(test)]
mod tests {
    extern crate test;
    use super::*;
    use rand::Rng;
    use test::Bencher;

    fn prep_list(num_of_nums: usize, target: i32, num_of_targets: i32) -> Vec<i32> {
        let mut rng = rand::thread_rng();
        let mut nums: Vec<i32> = Vec::new();

        nums.reserve(num_of_nums);
        for _x in 0..num_of_nums {
            nums.push(rng.gen::<i32>().abs() % 10000);
        }
        nums.retain(|&x| x != target);
        for _x in 0..num_of_targets {
            nums.push(target);
        }
        nums.sort();
        nums
    }

    #[bench]
    fn bench_prep_list(b: &mut Bencher) {
        let mut rng = rand::thread_rng();
        let mut nums: Vec<i32> = Vec::new();

        b.iter(|| {
            nums.reserve(10000000);
            for _x in 0..10000000 {
                nums.push(rng.gen::<i32>().abs() % 10000);
            }
            nums.retain(|&x| x != 42);
            for _x in 0..10000 {
                nums.push(42);
            }
            nums.sort();
        });
    }

    #[test]
    fn test_binary_search() {
        let (mut num, mut nums, mut times_present);

        num = 42;
        nums = prep_list(300, num, 4);
        times_present = nums.iter().filter(|&x| *x == num).count() as i64;
        assert_eq!(binary_search_range(&nums, num), times_present);

        num = 0;
        nums = prep_list(1000, num, 0);
        times_present = nums.iter().filter(|&x| *x == num).count() as i64;
        assert_eq!(binary_search_range(&nums, num), times_present);

        num = 0;
        nums = prep_list(0, num, 0);
        times_present = nums.iter().filter(|&x| *x == num).count() as i64;
        assert_eq!(binary_search_range(&nums, num), times_present);

        num = 24;
        nums = prep_list(100000000, num, 0);
        times_present = nums.iter().filter(|&x| *x == num).count() as i64;
        assert_eq!(binary_search_range(&nums, num), times_present);

        num = 9405;
        nums = prep_list(100000, num, 890);
        times_present = nums.iter().filter(|&x| *x == num).count() as i64;
        assert_eq!(binary_search_range(&nums, num), times_present);
    }

    #[bench]
    fn bench_binary_search_short(b: &mut Bencher) {
        let nums = prep_list(1000, 42, 14);
        b.iter(|| binary_search_range(&nums, 42));
    }

    #[bench]
    fn bench_binary_search_long(b: &mut Bencher) {
        let nums = prep_list(100000000, 7402, 100000);
        b.iter(|| binary_search_range(&nums, 7402));
    }
}
