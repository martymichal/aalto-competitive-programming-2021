#![feature(test)]

use std::collections::VecDeque;

fn dfs(graph: &Vec<Vec<usize>>) {
    let mut to_visit: Vec<usize> = Vec::new();
    let mut visited: Vec<usize> = Vec::new();

    if graph.len() <= 1 {
        return;
    }

    to_visit.push(1);

    while to_visit.len() > 0 {
        let curr = to_visit.pop().unwrap();

        if visited.contains(&curr) {
            continue;
        }

        println!("{}", curr);
        visited.push(curr);

        for edge in &graph[curr] {
            to_visit.push(*edge);
        }
    }
}

fn bfs(graph: &Vec<Vec<usize>>) {
    let mut to_visit: VecDeque<usize> = VecDeque::new();
    let mut visited: Vec<usize> = Vec::new();

    if graph.len() <= 1 {
        return;
    }

    to_visit.push_back(1);

    while to_visit.len() > 0 {
        let curr = to_visit.pop_front().unwrap();

        if visited.contains(&curr) {
            continue;
        }

        println!("{}", curr);
        visited.push(curr);

        for edge in &graph[curr] {
            to_visit.push_back(*edge);
        }
    }
}

fn main() {
    let mut graph: Vec<Vec<usize>> = Vec::new();

    let mut buf = String::new();
    loop {
        buf.clear();
        std::io::stdin()
            .read_line(&mut buf)
            .expect("couldn't read from stdin");
        buf = buf.trim().to_string();

        if buf.eq("END") {
            break;
        }

        let parts: Vec<&str> = buf.split_whitespace().collect();
        if parts.len() != 2 {
            std::process::exit(1);
        }

        let vert: usize = parts[0]
            .parse::<usize>()
            .expect("not a proper unsigned int");
        let edge: usize = parts[1]
            .parse::<usize>()
            .expect("not a proper unsigned int");

        if graph.len() <= vert {
            for _i in 0..=vert - graph.len() {
                graph.push(Vec::new());
            }
        }

        graph[vert].push(edge);
        graph[vert].sort();
    }

    println!("DFS");
    dfs(&graph);

    println!("\nBFS");
    bfs(&graph);
}

#[allow(soft_unstable)]
#[cfg(test)]
mod tests {
    extern crate test;
    use super::*;
    use test::Bencher;

    fn prep_graph(nodes: usize, edge_prob: f32) -> Vec<Vec<usize>> {
        let mut graph: Vec<Vec<usize>> = Vec::new();

        if nodes == 0 {
            return graph;
        }

        graph.reserve(nodes + 1);

        for i in 0..=nodes {
            graph.push(Vec::new());

            if i == 0 {
                continue;
            }

            if i == nodes {
                graph[i].push(1);
            } else {
                graph[i].push(i + 1);
            }

            let mut k = 0;
            while k < 4 && (rand::random::<f32>() % 1f32) < edge_prob {
                graph[i].push(rand::random::<usize>() % nodes);
                k += 1;
            }
        }

        graph
    }

    #[test]
    fn test_dfs_empty() {
        let graph = prep_graph(0, 0.0);
        dfs(&graph);
    }

    #[test]
    fn test_dfs_one() {
        let graph = prep_graph(1, 0.0);
        dfs(&graph);
    }

    #[test]
    fn test_dfs_basic() {
        let mut graph: Vec<Vec<usize>> = Vec::new();

        for _i in 0..=8 {
            graph.push(Vec::new());
        }

        graph[1].push(2);
        graph[1].push(3);
        graph[2].push(1);
        graph[3].push(1);
        graph[3].push(5);
        graph[4].push(2);
        graph[4].push(5);
        graph[4].push(6);
        graph[5].push(3);
        graph[5].push(4);
        graph[5].push(6);
        graph[6].push(5);
        graph[7].push(8);
        graph[8].push(7);

        dfs(&graph);
    }

    #[bench]
    fn test_dfs_huge(b: &mut Bencher) {
        let graph = prep_graph(1000000, 1.0);
        b.iter(|| dfs(&graph));
    }

    #[test]
    fn test_bfs_empty() {
        let graph = prep_graph(0, 0.0);
        bfs(&graph);
    }

    #[test]
    fn test_bfs_one() {
        let graph = prep_graph(1, 0.0);
        bfs(&graph);
    }

    #[test]
    fn test_bfs_basic() {
        let mut graph: Vec<Vec<usize>> = Vec::new();

        for _i in 0..=8 {
            graph.push(Vec::new());
        }

        graph[1].push(2);
        graph[1].push(3);
        graph[2].push(1);
        graph[3].push(1);
        graph[3].push(5);
        graph[4].push(2);
        graph[4].push(5);
        graph[4].push(6);
        graph[5].push(3);
        graph[5].push(4);
        graph[5].push(6);
        graph[6].push(5);
        graph[7].push(8);
        graph[8].push(7);

        bfs(&graph);
    }

    #[bench]
    fn test_bfs_huge(b: &mut Bencher) {
        let graph = prep_graph(1000000, 1.0);
        b.iter(|| bfs(&graph));
    }
}
