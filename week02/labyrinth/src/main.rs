fn get_index(cord: (usize, usize)) -> usize {
    cord.0 * cord.1
}

fn search_rec(
    graph: &mut Vec<Vec<(char, usize)>>,
    path: &mut Vec<char>,
    curr: (usize, usize),
    end: (usize, usize),
) -> bool {
    println!("{} {}", curr.0, curr.1);
    let mut found = false;
    let rows = graph.len();
    let cols = graph[0].len();

    if curr == end {
        return true;
    }

    // Find possible ways
    // LEFT
    if curr.1 >= 0 {
        let c = graph[curr.0][curr.1 - 1].0;
        if c == '.' || c == 'B' {
            path.push('L');
            found = search_rec(graph, path, (curr.0, curr.1 - 1), end);
            if !found {
                path.pop();
            }
        }
    }
    // RIGHT
    if curr.1 < cols {
        let c = graph[curr.0][curr.1 + 1].0;
        if c == '.' || c == 'B' {
            path.push('R');
            found = search_rec(graph, path, (curr.0, curr.1 + 1), end);
            if !found {
                path.pop();
            }
        }
    }
    // TOP
    if curr.0 >= 0 {
        let c = graph[curr.0 - 1][curr.1].0;
        if c == '.' || c == 'B' {
            path.push('T');
            found = search_rec(graph, path, (curr.0 - 1, curr.1), end);
            if !found {
                path.pop();
            }
        }
    }
    // BOTTOM
    if curr.0 < rows {
        let c = graph[curr.0 + 1][curr.1].0;
        if c == '.' || c == 'B' {
            path.push('D');
            found = search_rec(graph, path, (curr.0 + 1, curr.1), end);
            if !found {
                path.pop();
            }
        }
    }

    found
}

fn search(
    graph: &mut Vec<Vec<(char, usize)>>,
    start: (usize, usize),
    end: (usize, usize),
) -> Vec<char> {
    let mut path: Vec<char> = Vec::new();

    search_rec(graph, &mut path, start, end);

    path
}

fn main() {
    let mut buf = String::new();

    std::io::stdin()
        .read_line(&mut buf)
        .expect("couldn't read from stdin");

    let values: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    let (width, height) = (values[0], values[1]);
    let mut labyrinth: Vec<Vec<(char, usize)>> = vec![vec![(' ', 0); width]; height];
    let (mut start, mut end): ((i32, i32), (i32, i32)) = ((0, 0), (0, 0));
    for i in 0..height {
        buf.clear();
        std::io::stdin()
            .read_line(&mut buf)
            .expect("couldn't read from stdin");

        let chars = buf.chars().collect::<Vec<char>>();
        for k in 0..width {
            labyrinth[i][k] = (chars[k], 0);
            if chars[k] == 'A' {
                start = (i, k);
            }
            if chars[k] == 'B' {
                end = (i, k);
            }
        }
    }

    let path = search(&mut labyrinth, start, end);

    if path.len() != 0 {
        println!("YES");
        println!("{}", path.len());
        for step in path {
            print!("{}", step);
        }
    } else {
        println!("NO");
    }
}
