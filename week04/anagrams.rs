use std::collections::HashMap;

fn main() {
    let mut buf = String::new();
    let mut anagrams: HashMap<String, Vec<String>> = HashMap::new();
    let mut anagram_groups: i32 = 0;

    std::io::stdin()
        .read_line(&mut buf)
        .expect("could not read from stdin");
    let mut num_of_words: i32 = buf.trim().parse::<i32>().unwrap();

    while num_of_words > 0 {
        buf.clear();
        std::io::stdin().read_line(&mut buf).expect("could not read from stdin");
        buf = buf.trim().to_string();

        num_of_words -= 1;

        let mut buf_chars: Vec<char> = buf.chars().collect();
        buf_chars.sort();
        let sorted = buf_chars.iter().collect::<String>();

        if anagrams.contains_key(&sorted) {
            let entry = anagrams.entry(sorted).or_default();
            entry.push(buf.to_string());
            if entry.len() == 2 {
                anagram_groups += 1;
            }
        } else {
            let mut list = Vec::new();
            list.push(buf.to_string());
            anagrams.insert(sorted, list);
        }
    }

    println!("{}", anagram_groups);
    for (_anagram, list) in &anagrams {
        if list.len() == 1 {
            continue
        }
        println!("{}", list.len());
        for word in list {
            println!("{}", word);
        }
    }

}
