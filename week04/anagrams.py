#!/usr/bin/env python3

import itertools
import sys
import time


def main():
    num_of_words = int(input())
    num_of_groups = 0
    words = dict()

    for l in itertools.islice(sys.stdin, num_of_words):
        line = l.strip()
        line_sorted = sorted(line)
        if line_sorted not in words:
            words[line_sorted] = [line]
        else:
            words[line_sorted] += [line]
            if len(words[line_sorted]) == 2:
                num_of_groups += 1

    print(num_of_groups)
    for x in words.values():
        if len(x) < 2:
            continue

        sys.stdout.write(str(len(x)) + "\n" + "".join(str(s) + "\n" for s in x))


if __name__ == "__main__":
    main()
