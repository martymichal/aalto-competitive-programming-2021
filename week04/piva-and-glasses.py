from collections import defaultdict
 
def fac(n):
    n_fac = 1 
 
    for i in range(1, n + 1): 
        n_fac = n_fac * i
    return n_fac
 
word = input()
 
bags = defaultdict(list)
 
for letter in word:
    bags[letter].append(letter)
 
n_fac = fac(len(word))
 
division = 1
 
for group in bags:
    division = division * fac(len(bags[group]))
 
n_fac = int(n_fac // division)
 
print(n_fac)
