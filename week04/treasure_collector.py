import itertools
import sys


def main():
    i = input().split()
    size = int(i[0])
    treasure_count = int(i[1])

    treasures = dict()

    for l in itertools.islice(sys.stdin, treasure_count):
        xy = l.split()
        if int(xy[0]) not in treasures:
            treasures[int(xy[0])] = [int(xy[1])]
        else:
            treasures[int(xy[0])].append(int(xy[1]))

    curr_pos = (0, 0)
    curr_width = 0
    for row, cols in treasures.items():
        max = max(cols)
        min = min(cols)
        next_pos = (min, max)

        # First row with treasure
        if curr_width == 0:
            curr_pos = next_pos
            curr_width = curr_pos[1] - curr_pos[0] + 1
            continue

        to_leftmost = curr_poss[0] - next_pos[0]
        to_rightmost = curr_poss[1] - next_pos[1]

        if to_leftmost > 1:
            # leftmost is lower
        if to_leftmost < -1:
            # leftmost is higher
        if to_rightmost > 1:
            # rightmost is lower
        if to_rightmost < -1:
            # rightmost is higher

        curr_width = cur_pos[1] - cur_pos[0] + 1

    print(curr_width)


if __name__ == "__main__":
    main()
