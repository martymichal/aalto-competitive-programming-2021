#!/usr/bin/env python3


def main():
    bounds = input().split()
    left = int(bounds[0])
    right = int(bounds[1])
    months = 0
    curr = 1
    prev = 0

    while curr <= right:
        if curr >= left and curr <= right:
            months += 1

        old_curr = curr
        curr += prev
        prev = old_curr

    print(months, flush=True)


if __name__ == "__main__":
    main()
