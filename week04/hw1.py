#!/usr/bin/env python

"""
Write a program that reads from input 1 million integers, on one line, separated by spaces, and prints out the
sum of the numbers.
"""

def main():
    res = 0
    res = sum(list(map(int, input().split())))
    print(res)


if __name__ == "__main__":
    main()
