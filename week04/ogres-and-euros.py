canTake = True
 
while True:
    room = input().split()
 
    if not canTake or room[0] < room[1]:
        print("pass")
        canTake = True
    else:
        print("take")
        canTake = False
 
    if room[1] == "-1":
        break
