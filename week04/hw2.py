#!/usr/bin/env python

"""
Write a program that reads from input 1 million integers, each on its own line and prints out the sum of the numbers.
"""


def main():
    res = sum([int(input()) for x in range(1000000)])
    print(res)


if __name__ == "__main__":
    main()
