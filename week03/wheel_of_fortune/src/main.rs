fn calculate(graph: &Vec<(i128, usize)>) ->i128 {
    let len = graph.len();

    let (mut red, mut black, mut green): (i128, i128, i128) = (0, 0, 0);

    // .0 is ...0, .1 is ...1
    for k in 1..=3 {
        let mut mem: Vec<(i128, i128)> = vec![(graph[0].0, graph[1].0)];

        for j in 2..len+2 {
            let i = j % len;
            let lastmem = *mem.last().unwrap();
            let max = std::cmp::max(lastmem.1, lastmem.0 + graph[i].0);

            if lastmem.0 + graph[i].0 > lastmem.1 {
                // +
                mem.push((lastmem.0, max));
            } else {
                // -
                mem.push((lastmem.1, lastmem.0 + graph[i].0));
            }
        }

        let lastmem = *mem.last().unwrap();

        match k {
            1 => red = std::cmp::max(lastmem.0, lastmem.1),
            2 => black = std::cmp::max(lastmem.0, lastmem.1),
            3 => green = std::cmp::max(lastmem.0, lastmem.1),
            _ => panic!("WAAAAA"),
        }
    }

    std::cmp::max(red, std::cmp::max(black, green))
}

fn main() {
    let len;
    let mut sectors: Vec<(i128, usize)>;
    let money;

    let reader = std::io::stdin();
    let mut buf = String::new();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");

    len = buf
        .trim()
        .parse::<usize>()
        .expect("this is not a positive integer");

    buf.clear();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");

    sectors = buf
        .trim()
        .split_whitespace()
        .map(|x| (x.parse::<i128>().unwrap(), 0))
        .collect();

    assert_eq!(sectors.len(), len);

    money = calculate(&mut sectors);
    println!("{}", money);
}
