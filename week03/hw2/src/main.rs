fn distance_step(src: &str, a: usize, dest: &str, b: usize, table: &mut Vec<Vec<i64>>) -> i64 {
    let value;
    if src.as_bytes()[a] == dest.as_bytes()[b] {
        value = 0;
    } else {
        value = 1;
    }

    if a == 0 || b == 0 {
        table[a + 1][b + 1] = value;
        return value;
    }

    let dist = std::cmp::min(
        distance_step(src, a, dest, b - 1, table) + 1,
        std::cmp::min(
            distance_step(src, a - 1, dest, b, table) + 1,
            distance_step(src, a - 1, dest, b - 1, table) + value,
        ),
    );

    table[a + 1][b + 1] = dist;
    dist
}

fn distance(src: &str, dest: &str) -> (i64, Vec<Vec<i64>>) {
    let mut table: Vec<Vec<i64>> = vec![vec![0; dest.len() + 1]; src.len() + 1];

    if src.len() == 0 && dest.len() == 0 {
        return (0, table);
    }

    let a = src.len() - 1;
    let b = dest.len() - 1;

    (distance_step(src, a, dest, b, &mut table), table)
}

fn retrace(table: &Vec<Vec<i64>>) {
    let (mut r, mut c) = (0, 0);
    let (lim_r, lim_c) = (table.len(), table[0].len());

    for r in table.iter() {
        for c in r.iter() {
            print!("{}", c);
        }
        println!();
    }

    let mut curr = 0;
    while r != lim_r - 1 || c != lim_c - 1 {
        let (mut right, mut diag, mut down) = (i64::MAX, i64::MAX, i64::MAX);

        if c + 1 < lim_c {
            right = table[r][c + 1];
        }
        if r + 1 < lim_r {
            down = table[r + 1][c];
        }
        if r + 1 < lim_r && c + 1 < lim_c {
            diag = table[r + 1][c + 1];
        }
        let val;
        if curr == 0 {
            val = std::cmp::max(right, std::cmp::max(diag, down));
        } else {
            val = std::cmp::min(right, std::cmp::min(diag, down));
        }

        println!("{} {}", r, c);
        match val {
            x if x == diag => {
                r += 1;
                c += 1;
                if val > curr {
                    print!("C");
                }
            }
            x if x == right => {
                c += 1;
                if val > curr {
                    print!("A");
                }
            }
            x if x == down => {
                r += 1;
                if val > curr {
                    print!("D");
                }
            }
            _ => panic!("WAAAAAA"),
        }
        curr = val;
    }
}

fn main() {
    let src: String;
    let dest: String;

    let reader = std::io::stdin();
    let mut buf = String::new();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    src = buf.trim().to_string();

    buf.clear();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    dest = buf.trim().to_string();

    let (dist, table) = distance(&src, &dest);
    println!("\n{}\n", dist);
    retrace(&table)
}
