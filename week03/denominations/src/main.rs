fn main() {
    let num;
    let dest: Vec<u8>;

    let reader = std::io::stdin();
    let mut buf = String::new();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    num = buf
        .trim().parse::<usize>().unwrap();

    buf.clear();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    dest = buf
        .trim()
        .chars()
        .map(|x| x.to_digit(10).unwrap() as u8)
        .collect();



}
