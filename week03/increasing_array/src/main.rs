fn main() {
    let len;
    let mut arr: Vec<i128>;
    let mut num = 0;

    let reader = std::io::stdin();
    let mut buf = String::new();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");

    len = buf
        .trim()
        .parse::<usize>()
        .expect("this is not a positive integer");

    buf.clear();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");

    arr = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<i128>().unwrap())
        .collect();

    for i in 1..len {
        let diff = arr[i - 1] - arr[i];
        if diff > 0 {
            arr[i] += diff;
            num += diff;
        }
    }

    println!("{}", num);
}
