fn min_dom_set_rec(graph: &Vec<i128>, mem: &Vec<(i128, i128, i128)>) -> Vec<usize> {
    let mut set: Vec<usize> = Vec::new();

    set
}

fn min_dom_set(graph: &Vec<i128>) -> Vec<usize> {
    let len = graph.len();

    if len <= 1 {
        return min_dom_set_rec(graph, &vec![(0, 0, 0)]);
    }

    if len == 2 {
        if graph[1] > graph[0] {
            return min_dom_set_rec(graph, &vec![(0, 0, 0)]);
        }
        return min_dom_set_rec(graph, &vec![(0, 0, 0)]);
    }

    // .0 is ...1, .1 is ...10, .2 is ...100
    let mut mem: Vec<(i128, i128, i128)> = vec![(graph[0], graph[1], graph[2])];
    for i in 2..len {
        let lastmem = *mem.last().unwrap();
        println!("{} - {:?}", i, lastmem);

        let min = std::cmp::min(lastmem.0 + lastmem.2, std::cmp::min(lastmem.1, lastmem.2));

        mem.push((min, 0, 0));
    }

    min_dom_set_rec(graph, &mem)
}

fn min_vert_cov_rec(graph: &Vec<i128>, mem: &Vec<(i128, i128, i128)>) -> Vec<usize> {
    let mut set: Vec<usize> = vec![0];
    set
}

fn min_vert_cov(graph: &Vec<i128>) -> Vec<usize> {
    let len = graph.len();

    if len <= 1 {
        return min_dom_set_rec(graph, &vec![(0, 0, 0)]);
    }

    if len == 2 {
        if graph[1] > graph[0] {
            return min_dom_set_rec(graph, &vec![(0, 0, 0)]);
        }
        return min_dom_set_rec(graph, &vec![(0, 0, 0)]);
    }

    let mut mem: Vec<(i128, i128, i128)> = vec![(0, 0, 0)];

    min_vert_cov_rec(graph, &mem)
}

fn max_indep_set_extract(graph: &Vec<i128>, mem: &Vec<(i128, i128)>) -> Vec<usize> {
    let set: Vec<usize> = Vec::new();
    let len = set.len();

    /*
    for i in len-1..=2 {

    }*/

    set
}

fn max_indep_set(graph: &Vec<i128>) -> Vec<usize> {
    let len = graph.len();

    if len <= 1 {
        return max_indep_set_extract(graph, &vec![(0, graph[0])]);
    }

    let mut mem: Vec<(i128, i128)> = vec![(graph[0], graph[1])];
    // .0 is ...0, .1 is ...1
    for i in 2..len {
        let lastmem = *mem.last().unwrap();
        let max = std::cmp::max(lastmem.1, lastmem.0 + graph[i]);
        if lastmem.0 + graph[i] > lastmem.1 {
            // +
            print!("+ ");
            mem.push((lastmem.0, max));
        } else {
            // -
            print!("- ");
            mem.push((lastmem.1, lastmem.0 + graph[i]));
        }
        println!("G: {} - {:?} - {}", graph[i], lastmem, max);
        /*if lastmem.0 < lastmem.1 + graph[i] {
            if i - set[set.len() - 1] == 1 {
                set.pop();
            }
            set.push(i);
        }*/
    }

    max_indep_set_extract(graph, &mem)
}

fn main() {
    let len: usize;
    let mut graph: Vec<i128> = Vec::new();

    let reader = std::io::stdin();
    let mut buf = String::new();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");

    len = buf
        .trim()
        .parse::<usize>()
        .expect("this is not a positive integer");

    graph.reserve(len + 1);

    for _i in 0..len {
        let mut buf = String::new();
        reader
            .read_line(&mut buf)
            .expect("couldn't read from stdin");
        let weight = buf.trim().parse::<i128>().expect("not a 128-bit integer");
        graph.push(weight);
    }

    let indep_set = max_indep_set(&graph);
    println!("{:?}", indep_set);

    min_vert_cov(&graph);

    let dom_set = min_dom_set(&graph);
    println!("{:?}", dom_set);
}
