use std::collections::HashMap;

fn distance_step(
    src: &Vec<u8>,
    a: usize,
    dest: &Vec<u8>,
    b: usize,
    mem: &mut HashMap<Vec<u8>, i64>,
) -> i64 {
    let value;

    if mem.contains_key(src) {
        return *mem.get(src).unwrap();
    }

    let src_increm = src.iter().map(|x| (x + 1) % 10).collect::<Vec<u8>>();

    let insert = distance_step(src, a, dest, b - 1, mem) + 1;
    let delete = distance_step(src, a - 1, dest, b, mem) + 1;
    let change = distance_step(src, a - 1, dest, b - 1, mem) + value;
    let spin = distance_step(&src_increm, a, dest, b, mem);
    let dist = std::cmp::min(insert, std::cmp::min(delete, std::cmp::min(change, spin)));

    if src[a] == dest[b] {
        value = 0;
    } else {
        value = 1;
    }

    mem.insert(src.to_vec(), dist);

    dist
}

fn distance(src: &Vec<u8>, dest: &Vec<u8>) -> i64 {
    let a = src.len() - 1;
    let b = dest.len() - 1;

    let mut mem: HashMap<Vec<u8>, i64> = HashMap::new();

    distance_step(src, a, dest, b, &mut mem)
}

fn main() {
    let src: Vec<u8>;
    let dest: Vec<u8>;

    let reader = std::io::stdin();
    let mut buf = String::new();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    src = buf
        .trim()
        .chars()
        .map(|x| x.to_digit(10).unwrap() as u8)
        .collect();

    buf.clear();
    reader
        .read_line(&mut buf)
        .expect("couldn't read from stdin");
    dest = buf
        .trim()
        .chars()
        .map(|x| x.to_digit(10).unwrap() as u8)
        .collect();

    let dist = distance(&src, &dest);
    println!("{}", dist);
}
