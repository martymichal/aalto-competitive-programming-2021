use std::collections::HashMap;

fn update_edges(graph: &mut Vec<HashMap<usize, (u128, u128)>>, edge: (usize, usize), flow: u128) {
    let vals: (u128, u128);

    vals = graph[edge.0][&edge.1];
    graph[edge.0].insert(edge.1, (vals.0 - flow, vals.1 + flow));
}

fn main() {
    let reader = std::io::stdin();
    let mut buf = String::new();

    reader.read_line(&mut buf).expect("");

    let tmp: Vec<usize> = buf
        .trim()
        .split_whitespace()
        .map(|x| x.parse::<usize>().unwrap())
        .collect();
    let nodes = tmp[0];
    let edges = tmp[1];

    let mut graph: Vec<HashMap<usize, (u128, u128)>> = vec![HashMap::new(); nodes + 1];
    for _i in 0..edges {
        let mut buf = String::new();
        reader.read_line(&mut buf).expect("");

        let line: Vec<u128> = buf
            .trim()
            .split_whitespace()
            .map(|x| x.parse::<u128>().unwrap())
            .collect();

        let src = line[0] as usize;
        let dest = line[1] as usize;
        let speed = line[2];

        graph[src].insert(dest, (speed, 0));
    }

    let mut speed = 0;
    let mut scale: u128 = (10 as u128).pow(9);
    while scale > 0 {
        let mut path: Vec<(usize, usize)> = Vec::new();
        let mut to_visit: Vec<usize> = Vec::new();
        let mut visited: Vec<usize> = Vec::new();
        let mut lowest: u128 = u128::MAX;

        to_visit.push(1);

        let mut prev = usize::MAX;
        while to_visit.len() > 0 {
            let curr = to_visit.pop().unwrap();

            if visited.contains(&curr) {
                continue;
            }

            if visited.contains(&nodes) {
                break;
            }

            visited.push(curr);

            if prev != usize::MAX {
                path.push((prev, curr));
            }

            for edge in &graph[curr] {
                if edge.1 .0 > scale || edge.1.0 == 0 {
                    continue;
                }
                to_visit.push(*edge.0);
            }

            prev = curr;
        }

        if !visited.contains(&nodes) {
            scale /= 2;
            continue;
        }

        for edges in path.iter() {
            println!("{:?}", edges);
            let edge = graph[edges.0].get(&edges.1).unwrap();
            if lowest > edge.0 {
                lowest = edge.0;
            }
        }

        for edges in path.iter_mut() {
            update_edges(&mut graph, *edges, lowest);
        }

        speed += lowest;
    }

    println!("{}", speed);
}
