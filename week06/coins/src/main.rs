fn main() {
    let reader = std::io::stdin();
    let mut buf = String::new();

    reader.read_line(&mut buf).expect("");

    let mut num = buf.trim().parse::<usize>().unwrap();
    let mut cols: Vec<usize> = Vec::new();
    let mut i = 1;
    while num >= i {
        num -= i;
        cols.push(i);
        i += 1;
    }

    if num != 0 {
        let last = cols.last_mut().unwrap();
        *last += num;
    }

    println!("{}", cols.len());
    for col in cols {
        print!("{} ", col);
    }
}
